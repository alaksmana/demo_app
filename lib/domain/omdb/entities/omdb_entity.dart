import 'package:meta/meta.dart';

// class OmdbEntityRating {
//   final String source;
//   final String value;

//   OmdbEntityRating(this.source, this.value);
// }

class OmdbEntity {
  final String title;
  final String year;
  final String poster;
  final String imdbid;
  final String type;
  // final String rated;
  // final String released;
  // final String runtime;
  // final String genre;
  // final String director;
  // final String writer;
  // final String actors;
  // final String plot;
  // final String language;
  // final String country;
  // final String awards;
  // final List<OmdbEntityRating> ratings;
  // final String metascore;
  // final String imdbrating;
  // final String imdbvotes;
  // final String dvd;
  // final String boxoffice;
  // final String production;
  // final String website;
  // final String response;  

  OmdbEntity({
      @required this.title,
      @required this.year,
      @required this.poster,
      @required this.imdbid,
      @required this.type,
      // this.rated,
      // this.released,
      // this.runtime,
      // this.genre,
      // this.director,
      // this.writer,
      // this.actors,
      // this.plot,
      // this.language,
      // this.country,
      // this.awards,
      // this.ratings,
      // this.metascore,
      // this.imdbrating,
      // this.imdbvotes,      
      // this.dvd,
      // this.boxoffice,
      // this.production,
      // this.website,
      // this.response
    });
  
  OmdbEntity copyWith({
     String title,
     String year,
     String poster,
     String imdbid,
     String type,
  }) =>
      OmdbEntity(
        title: title ?? this.title,
        year: year ?? this.year,
        poster: poster ?? this.poster,
        imdbid: imdbid ?? this.imdbid,
        type: type ?? this.type,
      );
}

