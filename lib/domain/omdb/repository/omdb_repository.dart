import 'package:injectable/injectable.dart';
import 'package:demo_app/data/omdb/repository/omdb_repository.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';

@Bind.toType(OmdbRepositoryImpl)
@injectable
abstract class OmdbRepository {
  Future<List<OmdbEntity>> searchOmdbByTitleYear(String apikey, {String title, String year});
}