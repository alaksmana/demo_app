import 'package:injectable/injectable.dart';
import 'package:demo_app/data/favorite/repository/video_repository.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';

@Bind.toType(VideoRepositoryImpl)
@injectable
abstract class FavoriteVideoRepository {
  Future<void> addFavoriteVideo(String token, VideoEntity video);
}