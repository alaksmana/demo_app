import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:injectable/injectable.dart';
import 'package:demo_app/common/models/use_case.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';
import 'package:demo_app/domain/omdb/repository/video_repository.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetFavoriteVideoUsecase implements UseCase<void, Map<String, dynamic>> {
  final FavoriteVideoRepository favRepository;

  GetFavoriteVideoUsecase({@required this.favRepository});

  @override
  Future<void> call(Map payload) {
    //convert OmdbEntity to VideoEntity
    OmdbEntity omdb = payload['omdb'];

    VideoEntity newvideo = VideoEntity(id: omdb.imdbid, label: "", priority: 0, viewed: true, rating: 0, timestamp: DateTime.now().millisecondsSinceEpoch, title: omdb.title, year: omdb.year ?? DateTime.now().year.toString(), poster: omdb.poster);
    
    return favRepository.addFavoriteVideo(payload['token'], newvideo);
  }
}