import 'package:injectable/injectable.dart';
import 'package:demo_app/data/favorite/repository/video_repository.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';

@Bind.toType(VideoRepositoryImpl)
@injectable
abstract class VideoRepository {
  Future<List<VideoEntity>> getAllVideos(String token);
  Future<VideoEntity> getVideoById(String token, String id);
  Future<void> createVideo(String token, VideoEntity video);
  Future<void> updateVideo(String token, VideoEntity video);
  Future<void> deleteVideo(String token, VideoEntity video);
  Future<List<VideoEntity>> getRecommended(String token);
}