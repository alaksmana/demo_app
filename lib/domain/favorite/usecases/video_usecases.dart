import 'package:injectable/injectable.dart';
import 'package:demo_app/common/models/use_case.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:demo_app/domain/favorite/repository/video_repository.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetAllVideosUsecase implements UseCase<List<VideoEntity>, String> {
  final VideoRepository favoriteVideoRepository;

  GetAllVideosUsecase({@required this.favoriteVideoRepository});

  @override
  Future<List<VideoEntity>> call(String token) {
    return favoriteVideoRepository.getAllVideos(token);
  }
}

@lazySingleton
@injectable
class GetVideoByIdUsecase implements UseCase<VideoEntity, Map> {
  final VideoRepository favoriteVideoRepository;

  GetVideoByIdUsecase({@required this.favoriteVideoRepository});

  @override
  Future<VideoEntity> call(Map payload) {
    return favoriteVideoRepository.getVideoById(payload['token'], payload['id']);
  }
}

@lazySingleton
@injectable
class CreateVideoUsecase implements UseCase<void, Map> {
final VideoRepository favoriteVideoRepository;

  CreateVideoUsecase({@required this.favoriteVideoRepository});

  @override
  Future<void> call(Map payload) async {
    await favoriteVideoRepository.createVideo(payload['token'], payload['video']);
  }
}

@lazySingleton
@injectable
class UpdateVideoUsecase implements UseCase<void, Map> {
  final VideoRepository favoriteVideoRepository;

  UpdateVideoUsecase({@required this.favoriteVideoRepository});

  @override
  Future<void> call(Map payload) {
    return favoriteVideoRepository.updateVideo(payload['token'], payload['video']);
  }
}

@lazySingleton
@injectable
class DeleteVideoUsecase implements UseCase<void, Map> {
  final VideoRepository favoriteVideoRepository;

  DeleteVideoUsecase({@required this.favoriteVideoRepository});

  @override
  Future<void> call(Map payload) {
    return favoriteVideoRepository.deleteVideo(payload['token'], payload['video']);
  }
}

@lazySingleton
@injectable
class GetRecommendedUsecase implements UseCase<List<VideoEntity>, String> {
  final VideoRepository favoriteVideoRepository;

  GetRecommendedUsecase({@required this.favoriteVideoRepository});

  @override
  Future<List<VideoEntity>> call(String token) {
    return favoriteVideoRepository.getRecommended(token);
  }
}

