import 'package:demo_app/data/favorite/models/video_model.dart';
import 'package:meta/meta.dart';

class VideoEntity {
  final String id;
  final String label;
  final int priority;
  final bool viewed;
  final int rating;
  final int timestamp;
  final String title;
  final String year;
  final String poster;

  VideoEntity({
    @required this.id,
    @required this.label,
    @required this.priority,
    @required this.viewed,
    @required this.rating,
    @required this.timestamp,
    @required this.title,
    @required this.year,
    @required this.poster,
  });

  VideoEntity copyWith({
    String id,
    String label,
    int priority,
    bool viewed,
    int rating,
    int timestamp,
    String title,
    String year,
    String poster,
  }) =>
      VideoEntity(
        id: id ?? this.id,
        label: label ?? this.label,
        priority: priority ?? this.priority,
        viewed: viewed ?? this.viewed,
        rating: rating ?? this.rating,
        timestamp: timestamp ?? this.timestamp,
        title: title ?? this.title,
        year: year ?? this.year,
        poster: poster ?? this.poster,
      );

  static VideoEntity fromModel(Video model){
    return model;
  }

  static List<VideoEntity> fromModelList(List<Video> modelList){
    return modelList;
  }

}

