import 'package:demo_app/data/favorite/datasources/video_remote_datasource.dart';
import 'package:demo_app/domain/omdb/repository/video_repository.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:demo_app/data/favorite/datasources/video_local_datasource.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:demo_app/data/favorite/models/video_model.dart';
import 'package:demo_app/domain/favorite/repository/video_repository.dart';
import 'package:demo_app/common/network/network_check.dart';
import 'package:demo_app/common/errors/no_connection_error.dart';

@lazySingleton
@injectable
class VideoRepositoryImpl
    implements VideoRepository, FavoriteVideoRepository {
  final VideoLocalDatasource videoLocalDatasource;
  final VideoRemoteDatasource videoRemoteDatasource;
  final NetworkCheck networkCheck;

  VideoRepositoryImpl({
    @required this.videoLocalDatasource,
    @required this.videoRemoteDatasource,
    @required this.networkCheck,
  });

  @override
  Future<void> createVideo(String token, VideoEntity video) async {
    final localData = await videoLocalDatasource.getById(token, video.id);
    if(localData!=null){
      return;
    }
    if (await networkCheck.isOnline()) {
            
      await videoRemoteDatasource.create(token, Video.fromEntity(video));
      await videoLocalDatasource.create(token, Video.fromEntity(video));
    }
    else{
      throw NoConnectionError();    
    }
      
  }

  @override
  Future<void> deleteVideo(String token, VideoEntity video) async {
    if (await networkCheck.isOnline()) {
      await videoRemoteDatasource.delete(token, video as Video);
      await videoLocalDatasource.delete(token, video as Video);
    }
    else{throw NoConnectionError();}
  }

  @override
  Future<VideoEntity> getVideoById(String token, String id) async {
    final localData = await videoLocalDatasource.getById(token, id);
    Video newData;
    if(localData==null){
      if (await networkCheck.isOnline()) {
        newData = await videoRemoteDatasource.getById(token, id);
        if(newData!=null){
          await videoLocalDatasource.create(token, newData);
        }
        return newData;
      }
      else{
        throw NoConnectionError();    
      }
    }
    else{
      return localData;
    }
  }

  @override
  Future<List<VideoEntity>> getAllVideos(String token) async {
    if (await networkCheck.isOnline()) {
      return await videoRemoteDatasource.getAll(token);
    }
    else{
        throw NoConnectionError();    
      } 
  }

  @override
  Future<void> updateVideo(String token, VideoEntity video) async {
    Video v = Video.fromEntity(video);
    
    if (await networkCheck.isOnline()) {
        await videoRemoteDatasource.update(token, v);
        await videoLocalDatasource.update(token, v);
    } 
    else{
        throw NoConnectionError();    
    }
  }  

  @override
  Future<void> addFavoriteVideo(String token, VideoEntity video) async {
    await createVideo(token, video);
  }

  @override
  Future<List<VideoEntity>> getRecommended(String token) async {
    Video newData;
    int maxsize = 3;
    List<VideoEntity> recommendedList = new List<VideoEntity>();
    if (await networkCheck.isOnline()) {
      for(int i=0; i<maxsize; i++){
        newData = await videoRemoteDatasource.getRecommended(token); 
        recommendedList.add(newData) ;   
      } 
      return recommendedList;
    }
    else{
      throw NoConnectionError();    
    }
    
  }
}