import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:demo_app/data/favorite/models/video_model.dart';

import 'video_local_datasource.dart';

class VideoLocalDatasourceSharedPreferenceImpl
    implements VideoLocalDatasource {
  final SharedPreferences sharedPreferences;

  VideoLocalDatasourceSharedPreferenceImpl(
      {@required this.sharedPreferences});

  @override
  Future<List<Video>> getAll(String token) async {
    final jsonString = sharedPreferences.getString('video_cache_$token');
    if (jsonString != null) {
      List<dynamic> rawList = jsonDecode(jsonString);
      List<Video> videoList = rawList
          .map<Video>((dynamic rawItem) =>
              Video.fromJson(rawItem as Map<String, dynamic>))
          .toList();
          
      return videoList;
    }
    return null;
  }

  @override
  Future<void> setAll(String token, List<Video> videos) async{
    if (videos == null) {
      await sharedPreferences.remove('video_cache_$token');
    } else {
      final rawList = videos
          .map<Map<String, dynamic>>((Video video) => video.toJson())
          .toList();
          //.cast<Map<String, dynamic>>();
      final json = jsonEncode(rawList);
      await sharedPreferences.setString('video_cache_$token', json);
    }
  }

  @override
  Future<Video> getById(String token, String id) async{
    if(id!=null && id!=''){
      final jsonString = sharedPreferences.getString('video_cache_$token$id');
      if (jsonString != null) {
        dynamic rawItem = jsonDecode(jsonString);
        Video video = Video.fromJson(rawItem as Map<String, dynamic>);
        
        return video;
      }
      return null;
    }
    else{ return null;}
  }

  @override
  Future<void> create(String token, Video video) async{
    await sharedPreferences.setString('video_cache_$token${video.id}', jsonEncode(video));
  }

  @override
  Future<void> update(String token, Video video) async{
    if (video == null) {
      await sharedPreferences.remove('video_cache_$token${video.id}');
    } else {
      await sharedPreferences.setString('video_cache_$token${video.id}', jsonEncode(video));
    }
  }

  @override
  Future<void> delete(String token, Video video) async{
    if (video != null) {
      await sharedPreferences.remove('video_cache_$token${video.id}');
    }
  }

}
