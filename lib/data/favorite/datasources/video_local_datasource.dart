import 'package:injectable/injectable.dart';
import 'package:demo_app/data/favorite/models/video_model.dart';
import 'video_local_datasource_shared_preference_impl.dart';

@Bind.toType(VideoLocalDatasourceSharedPreferenceImpl)
@injectable
abstract class VideoLocalDatasource {
  Future<List<Video>> getAll(String token);
  Future<void> setAll(String token, List<Video> videos);
  Future<Video> getById(String token, String id);
  Future<void> create(String token, Video video);
  Future<void> update(String token, Video video);
  Future<void> delete(String token, Video video);
}

