
import 'dart:io';

import 'package:demo_app/data/favorite/models/video_model.dart';
import 'dart:convert';
import 'package:injectable/injectable.dart';
import 'package:demo_app/common/network/video_ws_client.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

@Bind.toType(VideoRemoteDatasourceImpl)
@injectable
abstract class VideoRemoteDatasource {
  Future<List<Video>> getAll(String token);
  Future<Video> getById(String token, String id);
  Future<void> create(String token, Video video);
  Future<void> update(String token, Video video);
  Future<void> delete(String token, Video video);
  Future<Video> getRecommended(String token);
}

@lazySingleton
@injectable
class VideoRemoteDatasourceImpl implements VideoRemoteDatasource {
  final VideoWsClient client;

  VideoRemoteDatasourceImpl({@required this.client});

  factory VideoRemoteDatasourceImpl.create() {
    return VideoRemoteDatasourceImpl(
      client: VideoWsClientImpl(http.Client()),
    );
  }

  @override
  Future<List<Video>> getAll(String token) async {
    
    Uri uri = Uri.https('demo-video-ws-chfmsoli4q-ew.a.run.app', 'video-ws/videos/');
    
    final response = await client.get(uri, headers:{HttpHeaders.contentTypeHeader : 'application/json', HttpHeaders.acceptHeader:'*/*', 'token' : token});
    String json = response.body;

    List<Video> videos = [];

    List<dynamic> decodedJson = jsonDecode(json);
    
    decodedJson.forEach((listItem) {
      // listItem.forEach((key, value) {
        videos.add(Video.fromJson(listItem as Map<String, dynamic>));
      // });
    });
    return videos;
  }

  @override
  Future<void> create(String token, Video video) async{
    Uri uri = Uri.https('demo-video-ws-chfmsoli4q-ew.a.run.app', 'video-ws/videos/');
    final response = await client.post(uri, headers:{HttpHeaders.contentTypeHeader : 'application/json', HttpHeaders.acceptHeader:'*/*', 'token' : token}, body:jsonEncode(video.toJson()));    
    // print(response);
    return null;
  }

  @override
  Future<void> delete(String token, Video video) async{
    Uri uri = Uri.https('demo-video-ws-chfmsoli4q-ew.a.run.app', 'video-ws/videos/${video.id}');
    final response = await client.delete(uri, headers:{HttpHeaders.contentTypeHeader : 'application/json', HttpHeaders.acceptHeader:'*/*', 'token' : token});

    return null;
  }

  @override
  Future<Video> getById(String token, String id) async{
    if(id!=null){
      Uri uri = Uri.https('demo-video-ws-chfmsoli4q-ew.a.run.app', 'video-ws/videos/$id');
      
      final response = await client.get(uri, headers:{HttpHeaders.contentTypeHeader : 'application/json', HttpHeaders.acceptHeader:'*/*', 'token' : token});
      if(response.statusCode==200){
        String json = response.body;

        Map<String,dynamic> decodedJson = jsonDecode(json);
        return Video.fromJson(decodedJson);
      }
    }
    return null;
  }

  @override
  Future<void> update(String token, Video video) async{
    Uri uri = Uri.https('demo-video-ws-chfmsoli4q-ew.a.run.app', 'video-ws/videos/${video.id}');
    final response = await client.put(uri, headers:{HttpHeaders.contentTypeHeader : 'application/json', HttpHeaders.acceptHeader:'*/*', 'token' : token}, body:jsonEncode(video));    

    return null;
  }

  @override
  Future<Video> getRecommended(String token) async{
    
    Uri uri = Uri.https('demo-video-ws-chfmsoli4q-ew.a.run.app', 'video-ws/recommended');
    
    final response = await client.get(uri, headers:{HttpHeaders.contentTypeHeader : 'application/json', HttpHeaders.acceptHeader:'*/*', 'token' : token});
    if(response.statusCode==200){
      String json = response.body;

      Map<String,dynamic> decodedJson = jsonDecode(json);
      return Video.fromJson(decodedJson);
    }
    
    return null;
  }
  
}

