import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';

class Video extends VideoEntity{
  
  Video({
    @required String id,
    @required String label,
    @required int priority,
    @required bool viewed,
    @required int rating,
    @required int timestamp,
    @required String title,
    @required String year,
    @required String poster
  }):super(
    id: id,
    label: label,
    priority: priority,
    viewed: viewed,
    rating: rating,
    timestamp: timestamp,
    title: title,
    year: year,
    poster: poster,
  );

  factory Video.fromJson(Map<String, dynamic> map) {
    String _yearcheck;
    if(map["year"]==null){
      _yearcheck = DateTime.now().year.toString();
    }
    else{
      if(map["year"] is int){
        _yearcheck = map["year"].toString();
      }
      else if(map["year"] is String){
        try {
          _yearcheck = map["year"];
        } catch (e) {
          _yearcheck = DateTime.now().year.toString();
        }
      }
    }

    return Video(
      id: map["id"],
      label: map["label"],
      priority: map["priority"],
      viewed: map["viewed"],
      rating: map["rating"],
      timestamp: map["timestamp"] ?? 0,
      title: map["title"],
      year: _yearcheck,
      poster: map["poster"]
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "label": label,
      "priority": priority,
      "viewed": viewed,
      "rating": rating,
      "timestamp": timestamp,
      "title": title,
      "year": year,
      "poster": poster
    };
  }

  static Video fromEntity(VideoEntity entity){
    Video result = new Video(id: entity.id, label: entity.label ?? "", priority: entity.priority ?? 0, viewed: entity.viewed ?? true, rating: entity.rating ?? 0, timestamp: entity.timestamp ?? DateTime.now().millisecondsSinceEpoch, title: entity.title ?? "", year: entity.year ?? "", poster: entity.poster ?? "");
    return result;
  }

  static List<Video> fromEntityList(List<VideoEntity> entityList){
    List<Video> resultList = entityList
          .map<VideoEntity>((dynamic entityItem) => Video.fromJson(entityItem as Map<String, dynamic>))
          .toList();
    return resultList;
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}
