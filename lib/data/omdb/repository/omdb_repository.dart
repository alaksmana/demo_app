import 'dart:async';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:demo_app/common/errors/no_connection_error.dart';
import 'package:demo_app/common/network/network_check.dart';
import 'package:demo_app/domain/omdb/repository/omdb_repository.dart';
import 'package:demo_app/data/omdb/datasources/omdb_remote_datasource.dart';
import 'package:demo_app/data/omdb/datasources/omdb_local_datasource.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';

@lazySingleton
@injectable
class OmdbRepositoryImpl
    implements OmdbRepository {
  final OmdbLocalDatasource omdbLocalDatasource;
  final OmdbRemoteDatasource omdbNetworkDatasource;
  final NetworkCheck networkCheck;

  OmdbRepositoryImpl({
    @required this.omdbLocalDatasource,
    @required this.omdbNetworkDatasource,
    @required this.networkCheck,
  });

  @override
  Future<List<OmdbEntity>> searchOmdbByTitleYear(String apikey, {String title, String year}) async {
    final localData = await omdbLocalDatasource.getOmdbsByTitleYear('$title$year');

    if (localData != null && localData.isNotEmpty) {
      return localData;
    } if (await networkCheck.isOnline()) {
      final omdbList = await omdbNetworkDatasource.searchOmdbByTitleYear(apikey, title: title, year:year);
      omdbLocalDatasource.setOmdbsByTitleYear('$title$year', omdbList);
      return omdbList;
    }
    // return [];
    throw NoConnectionError();
  }
}
