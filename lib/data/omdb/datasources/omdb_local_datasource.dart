import 'package:injectable/injectable.dart';
import 'package:demo_app/data/omdb/models/omdb_model.dart';
import 'package:demo_app/data/omdb/datasources/omdb_local_datasource_sharedpreference_impl.dart';

@Bind.toType(OmdbLocalDatasourceSharedPreferenceImpl)
@injectable
abstract class OmdbLocalDatasource {
  Future<List<Omdb>> getOmdbsByTitleYear(String titleyear);
  Future<void> setOmdbsByTitleYear(String titleyear, List<Omdb> omdbs);
}

