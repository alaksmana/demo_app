import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';

class Omdb extends OmdbEntity{

  Omdb({
    @required String title,
    @required String year,
    @required String poster,
    @required String imdbid,
    @required String type,
  }):super(
    title:title,
    year:year,
    poster:poster,
    imdbid:imdbid,
    type:type,
  );

  factory Omdb.fromJson(Map<String, dynamic> map) {
    return Omdb(
        title: map["Title"],
        year: map["Year"],
        poster: map["Poster"],
        imdbid: map["imdbID"],
        type: map["Type"],
        );
  }

  Map<String, dynamic> toJson() {
    return {
      "Title": title,
      "Year": year,
      "Poster": poster,
      "imdbID": imdbid,
      "Type": type,
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}

