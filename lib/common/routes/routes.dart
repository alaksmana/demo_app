class Routes {
  static const String initialPage = '/';
  static const String loginPage = '/login';
  static const String homePage = '/home';
  static const String dashboardPage = '/dashboard';
  static const String searchPage = '/search';
  static const String favoritePage = '/favorite';
  static const String favoriteDetailPage = '/favorite/detail';
  static const String settingsPage = '/settings';
}
