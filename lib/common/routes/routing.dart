import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:demo_app/presentation/dashboard/bloc/dashboard_bloc.dart';
import 'package:demo_app/presentation/dashboard/dashboard_page.dart';
import 'package:demo_app/presentation/favorite_details/bloc/detail_bloc.dart';
import 'package:demo_app/presentation/favorite_details/favorite_details_page.dart';
import 'package:flutter/material.dart';
import 'package:demo_app/common/routes/routes.dart';
import 'package:demo_app/presentation/home/home_page.dart';
import 'package:demo_app/presentation/login/login_page.dart';
import 'package:demo_app/presentation/splash/splash_page.dart';
import 'package:demo_app/presentation/search/search_page.dart';
import 'package:demo_app/presentation/favorite/favorite_page.dart';
import 'package:demo_app/presentation/settings/settings_page.dart';

import 'package:demo_app/common/config/injector.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:demo_app/presentation/search/bloc/omdb_bloc.dart';
import 'package:demo_app/presentation/search/bloc/video_bloc.dart';
import 'package:demo_app/presentation/favorite/bloc/video_bloc.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case Routes.initialPage:
        return MaterialPageRoute(
          builder: (_) => SplashScreenPage()
        );
      case Routes.loginPage:
        return MaterialPageRoute(
          builder: (_) => LoginPage(),
        );
      case Routes.homePage:
        return MaterialPageRoute(
          builder: (context) => _buildHomePage(args),
        );
      case Routes.dashboardPage:
        return MaterialPageRoute(
          builder: (context) => _buildDashboardPage(),
        );
      case Routes.searchPage:
        return MaterialPageRoute(
          builder: (context) => _buildSearchPage(),
        );
      case Routes.favoritePage:
        return MaterialPageRoute(
          builder: (context) => _buildFavoritePage(),
        );
      case Routes.favoriteDetailPage:
        return MaterialPageRoute(
          builder: (context) => _buildFavoriteDetailPage(args),
        );
      case Routes.settingsPage:
        return MaterialPageRoute(
          builder: (_) => SettingsPage(),
        );
        
      default:
        // return error route if named route was not found
        return _errorRoute();
    }
  }

  static Widget _errorWidget(){
    return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
  }

  // _errorRoute is just a screen with error.
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return _errorWidget();
    });
  }

  static Widget getRouteWidget(String routepath){
    switch (routepath) {
      case Routes.initialPage:
        return SplashScreenPage();
      case Routes.loginPage:
        return LoginPage();
      case Routes.homePage:
        return _buildHomePage(0);
      case Routes.dashboardPage:
        return _buildDashboardPage();
      case Routes.searchPage:
        return _buildSearchPage();
      case Routes.favoritePage:
        return _buildFavoritePage();
      case Routes.settingsPage:
        return SettingsPage();
        
      default:
        // return error route if named route was not found
        return _errorWidget();
    }
  }

  static Widget _buildSearchPage(){
    return MultiBlocProvider(
      providers: [
        BlocProvider<OmdbBloc>(
          create: (BuildContext context) => getIt<OmdbBloc>(),
        ),
        BlocProvider<FavoriteVideoBloc>(
          create: (BuildContext context) => getIt<FavoriteVideoBloc>(),
        ),
      ],
      child: SearchPage(),
    );
  }

  static Widget _buildFavoritePage(){
    return BlocProvider<FavoriteVideoListBloc>(
          create: (BuildContext context) => getIt<FavoriteVideoListBloc>(),
          child: FavoritePage(),
    );      
  }

  static Widget _buildFavoriteDetailPage(dynamic args){
    VideoEntity ve = args as VideoEntity;
    return BlocProvider<DetailBloc>(
          create: (BuildContext context) => getIt<DetailBloc>(),
          child: FavoriteDetailPage(inputVideo: ve),
    );      
  }

  static Widget _buildHomePage(dynamic args){
    int defaultIndex = args as int;
    return HomePage(inputIndex: defaultIndex);      
  }

  static Widget _buildDashboardPage(){
    return BlocProvider<DashboardBloc>(
          create: (BuildContext context) => getIt<DashboardBloc>(),
          child: DashboardPage(),
    );      
  }
  
}

