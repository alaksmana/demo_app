// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:demo_app/common/network/omdb_ws_client.dart';
import 'package:demo_app/data/favorite/datasources/video_local_datasource_shared_preference_impl.dart';
import 'package:demo_app/data/omdb/datasources/omdb_local_datasource_sharedpreference_impl.dart';
import 'package:http/src/client.dart';
import 'package:demo_app/common/network/network_check.dart';
import 'package:connectivity/connectivity.dart';
import 'package:demo_app/common/network/video_ws_client.dart';
import 'package:demo_app/data/favorite/datasources/video_local_datasource.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:demo_app/data/favorite/datasources/video_remote_datasource.dart';
import 'package:demo_app/data/favorite/repository/video_repository.dart';
import 'package:demo_app/data/omdb/datasources/omdb_local_datasource.dart';
import 'package:demo_app/data/omdb/datasources/omdb_remote_datasource.dart';
import 'package:demo_app/data/omdb/repository/omdb_repository.dart';
import 'package:demo_app/domain/favorite/repository/video_repository.dart';
import 'package:demo_app/domain/favorite/usecases/video_usecases.dart';
import 'package:demo_app/domain/omdb/repository/omdb_repository.dart';
import 'package:demo_app/domain/omdb/repository/video_repository.dart';
import 'package:demo_app/domain/omdb/usecases/omdb_usecases.dart';
import 'package:demo_app/domain/omdb/usecases/video_usecases.dart';
import 'package:demo_app/presentation/favorite/bloc/video_bloc.dart';
import 'package:demo_app/presentation/search/bloc/omdb_bloc.dart';
import 'package:demo_app/presentation/search/bloc/video_bloc.dart';
import 'package:demo_app/presentation/favorite_details/bloc/detail_bloc.dart';
import 'package:demo_app/presentation/dashboard/bloc/dashboard_bloc.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
void $initGetIt({String environment}) {
  getIt
    ..registerFactory<OmdbWsClient>(() => OmdbWsClientImpl(getIt<Client>()))
    ..registerLazySingleton<OmdbWsClientImpl>(
        () => OmdbWsClientImpl(getIt<Client>()))
    ..registerFactory<NetworkCheck>(
        () => NetworkCheckImpl(connectivity: getIt<Connectivity>()))
    ..registerLazySingleton<NetworkCheckImpl>(
        () => NetworkCheckImpl(connectivity: getIt<Connectivity>()))
    ..registerFactory<VideoWsClient>(() => VideoWsClientImpl(getIt<Client>()))
    ..registerLazySingleton<VideoWsClientImpl>(
        () => VideoWsClientImpl(getIt<Client>()))
    ..registerFactory<VideoLocalDatasource>(() =>
        VideoLocalDatasourceSharedPreferenceImpl(
            sharedPreferences: getIt<SharedPreferences>()))
    ..registerFactory<VideoRemoteDatasource>(
        () => VideoRemoteDatasourceImpl(client: getIt<VideoWsClient>()))
    ..registerLazySingleton<VideoRemoteDatasourceImpl>(
        () => VideoRemoteDatasourceImpl(client: getIt<VideoWsClient>()))
    ..registerLazySingleton<VideoRepositoryImpl>(() => VideoRepositoryImpl(
          videoLocalDatasource: getIt<VideoLocalDatasource>(),
          videoRemoteDatasource: getIt<VideoRemoteDatasource>(),
          networkCheck: getIt<NetworkCheck>(),
        ))
    ..registerFactory<OmdbLocalDatasource>(() =>
        OmdbLocalDatasourceSharedPreferenceImpl(
            sharedPreferences: getIt<SharedPreferences>()))
    ..registerFactory<OmdbRemoteDatasource>(
        () => OmdbRemoteDatasourceImpl(client: getIt<OmdbWsClient>()))
    ..registerLazySingleton<OmdbRemoteDatasourceImpl>(
        () => OmdbRemoteDatasourceImpl(client: getIt<OmdbWsClient>()))
    ..registerLazySingleton<OmdbRepositoryImpl>(() => OmdbRepositoryImpl(
          omdbLocalDatasource: getIt<OmdbLocalDatasource>(),
          omdbNetworkDatasource: getIt<OmdbRemoteDatasource>(),
          networkCheck: getIt<NetworkCheck>(),
        ))
    ..registerFactory<VideoRepository>(() => VideoRepositoryImpl(
          videoLocalDatasource: getIt<VideoLocalDatasource>(),
          videoRemoteDatasource: getIt<VideoRemoteDatasource>(),
          networkCheck: getIt<NetworkCheck>(),
        ))
    ..registerLazySingleton<GetAllVideosUsecase>(() =>
        GetAllVideosUsecase(favoriteVideoRepository: getIt<VideoRepository>()))
    ..registerLazySingleton<GetVideoByIdUsecase>(
        () => GetVideoByIdUsecase(favoriteVideoRepository: getIt<VideoRepository>()))
    ..registerLazySingleton<CreateVideoUsecase>(() => CreateVideoUsecase(favoriteVideoRepository: getIt<VideoRepository>()))
    ..registerLazySingleton<UpdateVideoUsecase>(() => UpdateVideoUsecase(favoriteVideoRepository: getIt<VideoRepository>()))
    ..registerLazySingleton<DeleteVideoUsecase>(() => DeleteVideoUsecase(favoriteVideoRepository: getIt<VideoRepository>()))
    ..registerLazySingleton<GetRecommendedUsecase>(() => GetRecommendedUsecase(favoriteVideoRepository: getIt<VideoRepository>()))
    ..registerFactory<OmdbRepository>(() => OmdbRepositoryImpl(
          omdbLocalDatasource: getIt<OmdbLocalDatasource>(),
          omdbNetworkDatasource: getIt<OmdbRemoteDatasource>(),
          networkCheck: getIt<NetworkCheck>(),
        ))
    ..registerFactory<FavoriteVideoRepository>(() => VideoRepositoryImpl(
          videoLocalDatasource: getIt<VideoLocalDatasource>(),
          videoRemoteDatasource: getIt<VideoRemoteDatasource>(),
          networkCheck: getIt<NetworkCheck>(),
        ))
    ..registerLazySingleton<GetOmdbUsecase>(() => GetOmdbUsecase(omdbRepository: getIt<OmdbRepository>()))
    ..registerLazySingleton<GetFavoriteVideoUsecase>(() => GetFavoriteVideoUsecase(favRepository: getIt<FavoriteVideoRepository>()))
    ..registerFactory<FavoriteVideoListBloc>(() => FavoriteVideoListBloc(getAllFavoriteVideosUsecase: getIt<GetAllVideosUsecase>(), deleteFavoriteVideoUsecase: getIt<DeleteVideoUsecase>()))
    ..registerFactory<OmdbBloc>(() => OmdbBloc(getOmdbUsecase: getIt<GetOmdbUsecase>()))
    ..registerFactory<FavoriteVideoBloc>(() => FavoriteVideoBloc(addFavoriteVideo: getIt<GetFavoriteVideoUsecase>()))
    ..registerFactory<DetailBloc>(() => DetailBloc(getVideoByIdUsecase: getIt<GetVideoByIdUsecase>(), updateVideoUsecase: getIt<UpdateVideoUsecase>()))
    ..registerFactory<DashboardBloc>(() => DashboardBloc(getRecommendedUsecase: getIt<GetRecommendedUsecase>()));
}
