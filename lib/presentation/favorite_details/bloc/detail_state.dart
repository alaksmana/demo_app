part of 'detail_bloc.dart';

@immutable
abstract class DetailState {}

class DetailInitialState extends DetailState {}

class DetailLoadingState extends DetailState {}

class DetailDataState extends DetailState {
  final VideoEntity currentFavoriteVideo;

  DetailDataState({this.currentFavoriteVideo});
}

class DetailErrorState extends DetailState {
  final dynamic error;

  DetailErrorState(this.error);
}