import 'package:cached_network_image/cached_network_image.dart';
import 'package:demo_app/common/routes/routes.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_keychain/flutter_keychain.dart';
import 'package:demo_app/common/utils/keychain.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

import 'bloc/detail_bloc.dart';

class FavoriteDetailPage extends StatefulWidget {
  final VideoEntity inputVideo;
  const FavoriteDetailPage({Key key, this.inputVideo}) : super(key: key);
  @override
  _FavoriteDetailPageState createState() => _FavoriteDetailPageState();
}

class _FavoriteDetailPageState extends State<FavoriteDetailPage> {

  String _token;
  VideoEntity _currentVideo;
  final labelController = TextEditingController();
  bool _isVideoModified = false;

  initKeychainApikey() async{
    var token = await FlutterKeychain.get(key: MyKeychain.TOKEN);
    if (mounted) {
      setState(() {
        _token = token;
        _currentVideo = widget.inputVideo;
        labelController.text = _currentVideo.label;
        _isVideoModified = false;
        BlocProvider.of<DetailBloc>(context).add(DetailRefreshEvent(_token, _currentVideo));
      });
    }
  }

  @override
  void initState() {
    super.initState();
    initKeychainApikey();
  }

  @override
  void dispose() {
    labelController.dispose();
    super.dispose();
  }

  void _SaveCurrentVideoEntity(){
    _currentVideo = _currentVideo.copyWith(label: labelController.text);
    _isVideoModified = false; //reset
    BlocProvider.of<DetailBloc>(context).add(DetailUpdateEvent(_token, _currentVideo));    
    Navigator.of(context).pop(true);    
  }

  Widget _showSaveFirstDialog(){
    if(_isVideoModified){
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Video Modified"),
            content: new Text("Current Video is modified and has not been saved!"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Quit without saving!"),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              ),
              new FlatButton(
                child: new Text("Cancel"),
                onPressed: () {                  
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    }
    else{
      Navigator.pop(context);
    }
  }

  void _showNetworkErrorDialog() {
    showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("No Connection"),
          content: new Text("Network connection error, try again later"),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  BlocProvider.of<DetailBloc>(context).add(DetailRefreshEvent(_token, _currentVideo));
                },
                child: Text('Retry')),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Cancel')),
          ],
        ));
  }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _FavoritePoster(String uri){
    if(uri==null || uri=='N/A'){
      return Icon(Icons.image);
    }
    else{
      return CachedNetworkImage(
        imageUrl: '$uri',
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
        fit: BoxFit.cover,
      );
    }
  }
  
  Widget _buildTopContent(BuildContext context, VideoEntity ve){
    _currentVideo = ve;
    final levelRating = Container(
      child: Container(
        child: 
        SmoothStarRating(
          allowHalfRating: false,
          onRatingChanged: (v) {
            if(mounted){
                setState(() {
                  _isVideoModified = true;
                  _currentVideo = _currentVideo.copyWith(rating: v.toInt());                  
              });
            }
          },
          starCount: 5,
          rating: _currentVideo.rating.toDouble(),
          size: 40.0,
          filledIconData: Icons.star,
          halfFilledIconData: Icons.star_half,
          defaultIconData: Icons.star_border,
          color: Colors.redAccent,
          borderColor: Colors.redAccent,
          spacing:0.0
        ),
      ),
    );

    final videoPriority = Container(
      padding: const EdgeInsets.all(7.0),
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5.0)),
      child: new Text(
        // "\$20",
        ve.priority.toString(),
        style: TextStyle(color: Colors.white),
      ),
    );

    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 60.0),
        Icon(
          Icons.movie,
          color: Colors.white,
          size: 40.0,
        ),
        Container(
          width: 90.0,
          child: new Divider(color: Colors.red),
        ),
        SizedBox(height: 10.0),
        Text(
          ve.title,
          style: TextStyle(color: Colors.white, fontSize: 35.0),
        ),
        SizedBox(height: 10.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(child: levelRating),
          ],
        ),
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(left: 10.0),
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new CachedNetworkImageProvider(
                  ve.poster,
                ),
                fit: BoxFit.cover,
              ),
            )),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: topContentText,
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              _showSaveFirstDialog();
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    );

    return topContent;
  }

  Widget _buildBottomContent(BuildContext context, VideoEntity ve){
    _currentVideo = ve;
    
    var tfLabel = TextFormField(
      controller: labelController,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Label',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    var ddPriority = Container(
        child: 
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
                Text('Priority '),
                SmoothStarRating(
                  allowHalfRating: false,
                  onRatingChanged: (v) {
                    if(mounted){
                        setState(() {
                          _isVideoModified = true;
                          _currentVideo = _currentVideo.copyWith(priority: v.toInt());                  
                      });
                    }
                  },
                  starCount: 5,
                  rating: _currentVideo.priority.toDouble(),
                  size: 30.0,
                  filledIconData: Icons.add_circle,
                  halfFilledIconData: Icons.add_circle_outline,
                  defaultIconData: Icons.add_circle_outline,
                  color: Colors.redAccent,
                  borderColor: Colors.redAccent,
                  spacing:0.0
                ),
            ],
          ),
      );

    var tbViewed = Container(
      child: 
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text('Viewed '),
              Switch(
                value: _currentVideo.viewed,
                onChanged: (value) {
                  if(mounted){
                    setState(() {
                      _isVideoModified = true;
                      _currentVideo = _currentVideo.copyWith(viewed: value);
                    });}
                },
                activeTrackColor: Colors.redAccent, 
                activeColor: Colors.red,
              ),
          ],
        ),
    );

    var dpDateTimeMovie = Container(
      child: 
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // Text(DateTime.fromMicrosecondsSinceEpoch(_currentVideo.timestamp).toString()),
            FlatButton(
              onPressed: () {
                  DatePicker.showDatePicker(context,
                                        showTitleActions: true,
                                        // minTime: DateTime(2018, 3, 5),
                                        // maxTime: DateTime(2019, 6, 7), 
                                        // onChanged: (date) {
                                        //   print('change $date');
                                        // },
                                        onConfirm: (date) {
                                          // print('confirm $date');
                                          if(mounted){
                                            setState(() {
                                              _isVideoModified = true;
                                              _currentVideo = _currentVideo.copyWith(timestamp: date.millisecondsSinceEpoch);
                                            });
                                          }
                                        }, 
                                        currentTime: DateTime.fromMillisecondsSinceEpoch(_currentVideo.timestamp), //, locale: LocaleType.id
                  );
              },
              child: Text(
                  DateTime.fromMillisecondsSinceEpoch(_currentVideo.timestamp).toString(),
                  style: TextStyle(color: Colors.red),
              )
          )
          ],
        )
      ,
    );

    final bottomContentText = 
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        tfLabel, 
        SizedBox(height: 10.0),
        ddPriority,
        tbViewed,
        dpDateTimeMovie
      ],
    );

    final saveButton = Container(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      width: MediaQuery.of(context).size.width,
      child: RaisedButton(
        onPressed: () => {
          _SaveCurrentVideoEntity()
        },
        color: Colors.red,
        child:
            Text("SAVE", style: TextStyle(color: Colors.white)),
      )
    );

    final bottomContent = Container(
      // height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      // color: Theme.of(context).primaryColor,
      padding: EdgeInsets.all(40.0),
      child: Center(
        child: Column(
          children: <Widget>[bottomContentText, saveButton],
        ),
      ),
    );

    return bottomContent;
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: 
      BlocConsumer<DetailBloc, DetailState>(
        listener: (context, state) {
          if (state is DetailErrorState) {
            _showNetworkErrorDialog();
          } else if (state is DetailLoadingState) {
            return _buildProgressBar();
          }
        },
        builder: (context, state) {

          if (state is DetailDataState) {
            
            return Scaffold(
                    body: Column(
                      children: <Widget>[
                        _buildTopContent(context, _isVideoModified || state.currentFavoriteVideo == null? _currentVideo : state.currentFavoriteVideo), 
                        _buildBottomContent(context, _isVideoModified || state.currentFavoriteVideo == null ? _currentVideo : state.currentFavoriteVideo),
                        ],
                    ),
                  );
          } else {
            return _buildProgressBar();
          }
        },
      ),
    );
  }
}