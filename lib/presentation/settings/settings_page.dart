import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:demo_app/common/utils/keychain.dart';

class SettingsPage extends StatefulWidget {  
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _showRecommended = true;

  void _initializeSharedPref() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if(mounted){
      setState(() {
        _showRecommended = prefs.getBool(MyKeychain.RECOMMENDED) ?? false;
      });
    }
  }

  void _setShowRecommended(bool value) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(MyKeychain.RECOMMENDED, value);
    if(mounted){
      setState(() {
        _showRecommended = value;
      });
    }
  }

  @override
  void initState() {
    _initializeSharedPref();
    super.initState();
  }

  Widget _buildRecommendedToggle(){
    return 
    Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child:
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Switch(
                  value: _showRecommended,
                  onChanged: (vx) {
                    _setShowRecommended(vx);
                  },
                  activeTrackColor: Colors.redAccent, 
                  activeColor: Colors.red,
                ),
                Text('Show Recommended Section'),
              ],),
          
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _buildRecommendedToggle(),
            ],
          ),
      )
    );
  }
}