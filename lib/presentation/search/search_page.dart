
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';
import 'package:flutter_keychain/flutter_keychain.dart';
import 'package:demo_app/common/utils/keychain.dart';

import 'bloc/omdb_bloc.dart';
import 'bloc/omdb_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'bloc/video_bloc.dart';

class SearchPage extends StatefulWidget {
  // final OmdbRepository omdbRepository;
  SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  
  String _searchtitle;
  String _searchyear;
  String _apikey;
  String _token;
  List<OmdbEntity> _omdbList;
  
  final titleController = TextEditingController();
  final yearController = TextEditingController();

  initKeychainApikey() async{
    var apikey = await FlutterKeychain.get(key: MyKeychain.APIKEY);
    var token = await FlutterKeychain.get(key: MyKeychain.TOKEN);
    if (mounted) {
      setState(() {
        _apikey = apikey;
        _token = token;
      });
    }
  }

  searchState(String searchtitle, String searchyear) {
    if (mounted) {
      setState(() {
        _searchtitle = searchtitle;
        _searchyear = searchyear;
        BlocProvider.of<OmdbBloc>(context)
                      .add(OmdbLoadByTitleYearEvent(_apikey ,title:_searchtitle, year:_searchyear));
      });
    }
  }

  setFavorite(OmdbEntity favOmdb){
    if (mounted) {
      setState(() {
        BlocProvider.of<FavoriteVideoBloc>(context)
                      .add(FavoriteVideoAddEvent(_token , favOmdb));
      });
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    titleController.dispose();
    yearController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initKeychainApikey();
    BlocProvider.of<OmdbBloc>(context).add(OmdbLoadTodayEvent());
  }

  void _showNetworkErrorDialog() {
    showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("No Connection"),
          content: new Text("Network connection error, try again later"),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  BlocProvider.of<OmdbBloc>(context)
                      .add(OmdbLoadByTitleYearEvent(_apikey ,title:_searchtitle, year:_searchyear));
                },
                child: Text('Retry')),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Cancel')),
          ],
        ));
  }

  // void _launchURL(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(url);
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _omdbPoster(String uri){
    if(uri==null || uri=='N/A'){
      return Icon(Icons.image);
    }
    else{
      return CachedNetworkImage(
        imageUrl: '$uri',
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
        fit: BoxFit.cover,
      );
    }
  }

  //using omdbs build list view and card
  Widget _buildListView(List<OmdbEntity> omdbs) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: ListView.builder(
        itemBuilder: (context, index) {
          OmdbEntity omdb = omdbs[index];
          
          return Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Flexible(child: 
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                omdb.title,
                                style: Theme.of(context).textTheme.title,
                              ),
                              Text(
                                'Year: ${omdb.year}',
                                style: Theme.of(context).textTheme.body1,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 30),
                        Flexible(
                          child: _omdbPoster(omdb.poster),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.favorite),
                          tooltip: 'Set Favorite Movie',
                          onPressed: () {
                            setFavorite(omdb);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        },
        itemCount: omdbs.length,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    
  var tfTitle = TextFormField(
    controller: titleController,
    keyboardType: TextInputType.text,
    autofocus: false,
    decoration: InputDecoration(
      hintText: 'Movie Title',
      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    ),
  );

  var tfYear = TextFormField(
    controller: yearController,
    keyboardType: TextInputType.text,
    autofocus: false,
    decoration: InputDecoration(
      hintText: 'Movie Year',
      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    ),
  );

  var searchBar =  
    Container(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: tfTitle,
          ),
          SizedBox(width: 10.0),
          Expanded(
            child: tfYear,
          ),
          IconButton(
            icon: Icon(Icons.search),
            tooltip: 'Search Movie',
            onPressed: () {
              searchState(titleController.text, yearController.text);
              FocusScope.of(context).requestFocus(FocusNode());
            },
          ),
        ],
      )
    );

    return SafeArea(
      child: 
      BlocConsumer<OmdbBloc, OmdbState>(
        listener: (context, state) {
          if (state is OmdbErrorState) {
            _showNetworkErrorDialog();
          } else if (state is OmdbLoadingState) {
            if(mounted){
                setState(() {
                  _searchtitle = state.title;
                  _searchyear = state.year;
                  _apikey = state.apikey;
              });
            }
          }
        },
        builder: (context, state) {
          if (state is OmdbDataState) {
            _omdbList = state.omdbList;
            return Column(
                    children: <Widget>[
                      Expanded(child: AnimatedSwitcher(
                          duration: const Duration(milliseconds: 300),
                          child: _buildListView(_omdbList),
                        )),
                      searchBar,
                    ],
                  );
          } else {
            return _buildProgressBar();
          }
        },
      ),
    );
  }

}