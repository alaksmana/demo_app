part of 'video_bloc.dart';

@immutable
abstract class FavoriteVideoState {}

@immutable
class FavoriteVideoInitialState extends FavoriteVideoState {}


@immutable
class FavoriteVideoLoadingState extends FavoriteVideoState {
  final String token;

  FavoriteVideoLoadingState(this.token);
}

@immutable
class FavoriteVideoDataState extends FavoriteVideoState {}

@immutable
class FavoriteVideoErrorState extends FavoriteVideoState {
  final dynamic error;

  FavoriteVideoErrorState(this.error);
}


