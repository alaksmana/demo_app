part of 'omdb_bloc.dart';

@immutable
abstract class OmdbEvent {}

@immutable
class OmdbLoadTodayEvent extends OmdbEvent {
}

@immutable
class OmdbLoadByTitleYearEvent extends OmdbEvent {
  final String apikey;
  final String title;
  final String year;

  OmdbLoadByTitleYearEvent(this.apikey,{this.title, this.year});
}

