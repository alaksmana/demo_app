import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';
import 'package:demo_app/domain/omdb/usecases/video_usecases.dart';

part 'video_event.dart';
part 'video_state.dart';

@injectable
class FavoriteVideoBloc extends Bloc<FavoriteVideoEvent, FavoriteVideoState> {
  final GetFavoriteVideoUsecase addFavoriteVideo;

  FavoriteVideoBloc({@required this.addFavoriteVideo});

  @override
  FavoriteVideoState get initialState => FavoriteVideoInitialState();

  @override
  Stream<FavoriteVideoState> mapEventToState(
    FavoriteVideoEvent event,
  ) async* {
    if(event is FavoriteVideoAddEvent){
      yield* _favVideoOmdb(event.token, event.omdb);
    }
    else{
      yield FavoriteVideoDataState();
    }
  }

  Stream<FavoriteVideoState> _favVideoOmdb(String token, OmdbEntity omdb) async* {
    yield FavoriteVideoLoadingState(token);
    try {
      Map<String, dynamic> payload = {'token':token, 'omdb':omdb};
      await addFavoriteVideo(payload);
      yield FavoriteVideoDataState();
    } catch (e) {
      yield FavoriteVideoErrorState(e);
    }
  }
}
