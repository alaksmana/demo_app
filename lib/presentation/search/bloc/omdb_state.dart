part of 'omdb_bloc.dart';

@immutable
abstract class OmdbState {}

@immutable
class OmdbInitialState extends OmdbState {}


@immutable
class OmdbLoadingState extends OmdbState {
  final String apikey;
  final String title;
  final String year;

  OmdbLoadingState(this.apikey, {this.title, this.year});
}

@immutable
class OmdbDataState extends OmdbState {
  final List<OmdbEntity> omdbList;

  OmdbDataState({this.omdbList});
}

@immutable
class OmdbErrorState extends OmdbState {
  final dynamic error;

  OmdbErrorState(this.error);
}


