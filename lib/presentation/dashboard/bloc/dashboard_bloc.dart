import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:demo_app/common/models/use_case.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:demo_app/domain/favorite/usecases/video_usecases.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

@injectable
class DashboardBloc
    extends Bloc<DashboardEvent, DashboardState> {
  final GetRecommendedUsecase getRecommendedUsecase;

  DashboardBloc({
    this.getRecommendedUsecase,
  });

  @override
  DashboardState get initialState =>
      DashboardInitialState();

  @override
  Stream<DashboardState> mapEventToState(
    DashboardEvent event,
  ) async* {
    if (event is DashboardRefreshEvent) {
      yield DashboardLoadingState();
      try {
        final String token = event.token;
        
        final liveVideo = await getRecommendedUsecase(token);
        yield DashboardDataState(recommendedList: liveVideo);
      } catch (e) {
        yield DashboardErrorState(e);
      }
    } 
  }
}
