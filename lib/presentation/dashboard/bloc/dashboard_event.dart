part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardEvent {}

@immutable
class DashboardRefreshEvent extends DashboardEvent{
  final String token;
  
  DashboardRefreshEvent(this.token);
}


