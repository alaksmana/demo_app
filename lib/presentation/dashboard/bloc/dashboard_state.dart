part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardState {}

class DashboardInitialState extends DashboardState {}

class DashboardLoadingState extends DashboardState {}

class DashboardDataState extends DashboardState {
  final List<VideoEntity> recommendedList;

  DashboardDataState({this.recommendedList});
}

class DashboardErrorState extends DashboardState {
  final dynamic error;

  DashboardErrorState(this.error);
}