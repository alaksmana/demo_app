// import 'package:demo_app/routes/routing.dart';
import 'package:flutter/material.dart';
import 'package:demo_app/common/routes/routes.dart';
import 'dart:async';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:package_info/package_info.dart';


class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {

  String _appName;
  // String _packageName;
  String _version;
  // String _buildNumber;


  @override
  void initState() { 
    super.initState();
    startSplashScreen();
  }

  startSplashScreen() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    if (mounted) {
      setState(() {
        _appName = packageInfo.appName ?? 'demo_app';
        // _packageName = packageInfo.packageName;
        _version = packageInfo.version ?? '0.0.0';
        // _buildNumber = packageInfo.buildNumber;
      });
    }

    var duration = const Duration(seconds: 5);

    return Timer(duration, () {
      Navigator.of(context).pushReplacementNamed(Routes.loginPage);
    });
  }

  @override
  Widget build(BuildContext context) {
    
  return Scaffold(
    backgroundColor: Colors.red,
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SvgPicture.asset(
            "images/logo.svg",
            width: 200.0,
            height: 100.0,
            fit: BoxFit.cover,
          ),
          Text('$_appName'),
          Text('$_version'),
        ]
      )
    ),
  );
  }
}