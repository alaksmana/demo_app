part of 'video_bloc.dart';

@immutable
abstract class VideoListEvent {}

@immutable
class VideoListRefreshEvent extends VideoListEvent{
  final String token;
  
  VideoListRefreshEvent(this.token);
}

@immutable
class VideoListDeleteEvent extends VideoListEvent{
  final String token;
  final VideoEntity favoriteVideoEntity;

  VideoListDeleteEvent(this.token, this.favoriteVideoEntity);
}

