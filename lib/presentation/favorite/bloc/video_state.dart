part of 'video_bloc.dart';

@immutable
abstract class FavoriteVideoListState {}

class FavoriteVideoListInitialState extends FavoriteVideoListState {}

class FavoriteVideoListLoadingState extends FavoriteVideoListState {}

class FavoriteVideoListDataState extends FavoriteVideoListState {
  final List<VideoEntity> favoriteVideoList;

  FavoriteVideoListDataState({this.favoriteVideoList});
}

class FavoriteVideoListDeleteDataState extends FavoriteVideoListState{
  final List<VideoEntity> favoriteVideoList;

  FavoriteVideoListDeleteDataState({this.favoriteVideoList});
}

class FavoriteVideoListErrorState extends FavoriteVideoListState {
  final dynamic error;

  FavoriteVideoListErrorState(this.error);
}