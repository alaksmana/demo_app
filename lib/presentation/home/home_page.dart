import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_keychain/flutter_keychain.dart';
import 'package:demo_app/common/routes/routes.dart';
import 'package:demo_app/common/routes/routing.dart';
import 'package:demo_app/common/utils/keychain.dart';

class HomePage extends StatefulWidget {
  final int inputIndex;
  const HomePage({Key key, this.inputIndex}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  String _apikey;
  String _token;
  String _title;

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  static List<Widget> _widgetOptions = <Widget>[
    RouteGenerator.getRouteWidget(Routes.dashboardPage),
    RouteGenerator.getRouteWidget(Routes.searchPage),
    RouteGenerator.getRouteWidget(Routes.favoritePage),
    RouteGenerator.getRouteWidget(Routes.settingsPage),
  ];

  void _onItemTapped(int index) {
    if (mounted) {
      setState(() {
        switch (index) {
          case 0: _title = 'Dashboard';            
            break;
          case 1: _title = 'Search';
            break;
          case 2: _title = 'Favorite';
            break;
          case 3: _title = 'Settings';
            break;
          default:_title = 'demo_app';
        }
        _selectedIndex = index;
      });
    }
  }

  isKeyTokenExist() async {
    var apikey = await FlutterKeychain.get(key: MyKeychain.APIKEY);
    var token = await FlutterKeychain.get(key: MyKeychain.TOKEN);

    //key and token exist, reuse them
    if (apikey != null && token != null) {
      if (mounted) {
        setState(() {
          _selectedIndex = widget.inputIndex;
          _apikey = apikey;
          _token = token;
        });
      }
      return true;
    } else {
      //get from textfield
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    isKeyTokenExist();
  }

  @override
  Widget build(BuildContext context) {
    final drawercontent = Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        // padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SvgPicture.asset('images/logo.svg', width: 50, height: 50),
                Text(' Navigate to '),
              ],
            ),
            decoration: BoxDecoration(
              color: Colors.red,
            ),
          ),
          ListTile(
            title: Text('Dashboard'),
            onTap: () {
              _onItemTapped(0);
                Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Search'),
            onTap: () {
              _onItemTapped(1);
                Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Favorite'),
            onTap: () {
              _onItemTapped(2);
                Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Setting'),
            onTap: () {
              _onItemTapped(3);
                Navigator.pop(context);
            },
          ),
        ],
      ),
    );

    final bottombar = BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.dashboard),
          title: Text('Dashboard'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.search),
          title: Text('Search'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite),
          title: Text('Favorite'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          title: Text('Settings'),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.red.shade800,
      unselectedItemColor: Colors.grey,
      onTap: _onItemTapped,
    );

    return Scaffold(
      appBar: AppBar(title: Text(_title ?? 'demo_app')),
      drawer: drawercontent,
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: bottombar,
    );
  }
}
